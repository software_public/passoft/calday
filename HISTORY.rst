=======
History
=======

2018.138 (2018-06-07)
------------------

* First release on new build system.

2020.189 (2020-07-07)
------------------
* Updated to work with Python 3 (resources used: 2to3, caniusepython3, pylint,
  flak8, mypy)
* Added some unit tests to ensure basic functionality of calday
* Updated list of platform specific dependencies to be installed when
  installing calday in dev mode (see setup.py)
* Installed and tested calday  against Python3.[5,6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for "calday" that can run on Python3.[5,6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[5,6,7,8]
  in GitLab CI pipeline
