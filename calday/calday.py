#!/usr/bin/env python3

"""
calday.py given a julian date will convert the date into its equivalent
calendar date and prints both.

# Shannen Lowe
# 05-18-2018

Maeva Pourpoint
July 2020
Updates to work under Python 3.
Unit tests to ensure basic functionality.
Code cleanup to conform to the PEP8 style guide.
Directory cleanup (remove unused files introduced by Cookiecutter).
Packaged with conda.
"""

import sys
import datetime


def usage():
    """Prints the usage."""
    print('Usage : {} day_of_year [year]'.format(sys.argv[0]))


def julian_to_cal(argv):
    """
    Takes the julian date given and prints both the julian date and it's
    calendar date equivalent.
    """
    day = argv[0]
    if len(argv) == 1:
        # If no year is given, use current year.
        yr = datetime.datetime.today().strftime("%Y")
    else:
        yr = argv[1]

    julian_date = '{} {}'.format(day.zfill(3), yr.zfill(4))

    if is_date_valid(julian_date):
        print('\n Julian Date {}'.format(julian_date))
        # create datetime object
        # %j The day number of the year [001,366]
        # %Y The year, including the century. [0001,9999]
        d = datetime.datetime.strptime(julian_date, "%j %Y")
        # Format d to a string that can be printed.
        calendar_date = d.strftime("%m %d %Y")
        print(' Calendar Date {}\n'.format(calendar_date))

    else:
        if int(yr) < 0 or int(yr) > 9999:
            print(' ERROR Year Range [0001 - 9999]')
        else:
            print(' ERROR Year {} does not have {} days'.format(yr, day))
        sys.exit(1)


def is_date_valid(julian_date):
    """Check if the date of the year is valid."""
    try:
        # strptime will raise an error if date or year is out of range
        datetime.datetime.strptime(julian_date, "%j %Y")
        return True
    except ValueError:
        return False


def main():
    if len(sys.argv[1:]) < 1 or len(sys.argv[1:]) > 2:
        usage()
        sys.exit(1)
    julian_to_cal(sys.argv[1:])


if __name__ == "__main__":
    main()
