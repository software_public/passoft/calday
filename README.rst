======
calday
======

* Description: displays current calendar day

* Usage: calday julian_day
         calday julian_day year

* Free software: GNU General Public License v3 (GPLv3)
