#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `calday` package."""

import io
import unittest

from unittest.mock import patch
from calday.calday import is_date_valid, julian_to_cal

VALID_DATE = ['92', '1990']
UNVALID_DAY = ['370']
UNVALID_YEAR = ['72', '-2050']


class TestCalday(unittest.TestCase):
    """Tests for `calday` package."""

    def test_is_date_valid(self):
        """Test basic functionality of is_date_valid function"""
        julian_date_valid = "{} {}".format(VALID_DATE[0], VALID_DATE[1])
        julian_date_unvalid_1 = "{}".format(UNVALID_DAY[0])
        julian_date_unvalid_2 = "{} {}".format(UNVALID_YEAR[0],
                                               UNVALID_YEAR[1])
        self.assertTrue(is_date_valid(julian_date_valid),
                        "{} is not a valid date".format(julian_date_valid))
        self.assertFalse(is_date_valid(julian_date_unvalid_1),
                         "{} is a valid date".format(julian_date_unvalid_1))
        self.assertFalse(is_date_valid(julian_date_unvalid_2),
                         "{} is a valid date".format(julian_date_unvalid_2))

    @patch('calday.calday.sys.exit', autospec=True)
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_julian_to_cal(self, mock_stdout, mock_exit):
        """Test basic functionality of julian_to_cal function"""
        julian_to_cal(VALID_DATE)
        output = '\n Julian Date 092 1990\n Calendar Date 04 02 1990\n\n'
        self.assertEqual(mock_stdout.getvalue(), output,
                         "Failed to properly convert date!")
        julian_to_cal(UNVALID_DAY)
        self.assertTrue(mock_exit.called, "sys.exit(1) never called - "
                        "julian_to_cal() not fully exercised!")
        julian_to_cal(UNVALID_YEAR)
        self.assertTrue(mock_exit.called, "sys.exit(1) never called - "
                        "julian_to_cal() not fully exercised!")
